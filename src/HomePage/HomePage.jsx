import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Select from 'react-select';
import config from 'config';
import { authHeader } from '../_helpers';

import { userActions } from '../_actions';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        const { user } = this.props;
        this.state = {
            adUnit:{
                title: '',
                content: '',
                image: '',
                video: '',
                url: '',
                createdby: user.username,
                interests: [],
                viewcallbackurl: '',
                likecallbackurl: '',
                clickcallbackurl: '',
                basepayoutv: 0,
                basepayoutc: 0,
                basepayoutl: 0,
            },
            adUnits: [],
            posts:[],
            adUnitsBOUI:[{
                adID: "sd",
                title: "desc",
                basepayout: 23
            }]
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDDChange = this.handleDDChange.bind(this);
        this.getAddUnits = this.getAddUnits.bind(this);

    }

    handleDDChange(value) {
        const { adUnit } = this.state;
        
        this.setState({
            adUnit: {
                ...adUnit,
                interests: value
            }
        });
    }

    componentDidMount() {
        // this.props.getUsers();

        this.getAddUnits();
        this.getAdUnitsBOI();
    }

    getAddUnits() {
        fetch(`${config.adunitsApiUrl}/adunits/getAllAdunits`, {
            method: 'GET',
            headers: authHeader()
            })
            .then(r => r.json())
            .then(data => {
                this.setState({
                    adUnits : data
                })
                console.log('data returned:', data)
            });
    }

    handleDeleteUser(id) {
        return (e) => this.props.deleteUser(id);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { adUnit } = this.state;
        this.setState({
            adUnit: {
                ...adUnit,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { adUnit } = this.state;
        const processedAdUnit = {...adUnit};
        processedAdUnit.interests = processedAdUnit.interests.map((item, index) => item.value)

        const t=this;
        fetch(`${config.adunitsApiUrl}/adunits/createAdunit`, {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(processedAdUnit)
          })
            .then(r => r)
            .then(data => {
                if(data.ok) {
                    t.getAddUnits();
                   console.log("ad unit created successfully.")
                }
            });
    }

    getAdUnitsBOI() {
        const { user } = this.props;
        fetch(`${config.adunitsApiUrl}/adunits/getAdunitsBUI`, {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify({"interests":user.interests})
            })
            .then(r => r.json())
            .then(data => {
                this.setState({
                    adUnitsBOUI : data
                })
            });
    }
   

    render() {
        const { user, users } = this.props;
        const { adUnit } = this.state;
        const { interests } = this.state.adUnit;
        const interestsList = [
            { label: "Adventure", value: "Adventure" },
            { label: "Art", value: "Art" },
            { label: "Pets", value: "Pets" },
            { label: "Books", value: "Books" },
            { label: "Education", value: "Education" },
            { label: "Fashion", value: "Fashion" },
            { label: "Fitness", value: "Fitness" },
            { label: "Food", value: "Food" },
            { label: "Gadgets", value: "Gadgets" },
            { label: "Gardening", value: "Gardening" },
            { label: "Games", value: "Games" },
            { label: "Health", value: "Health" },
            { label: "Innovation", value: "Innvation" },
            { label: "Jewellery", value: "Jewellery" },
            { label: "Knowledge", value: "Knowledge" },
            { label: "Learning", value: "Learning" },
            { label: "Music", value: "Music" },
            { label: "Nutrition", value: "Nutrition" },
            { label: "Outdoor", value: "Outdoor" },
            { label: "Photography", value: "Photography" },
            { label: "Sports", value: "Sports" },
            { label: "Technology", value: "Technology" },
            { label: "Travelling", value: "Travelling" },
            { label: "Writing", value: "Writing" },
          ];
        return (
            
            <div>
                
                <div className="jumbotron" style={{"padding":"20px"}} >
                    <h1 className="float-left">Network Tap</h1>
                    <Link className="float-right" to="/login">Logout</Link>
                    <p className="float-right" style={{"marginRight":"20px"}} >Hi {user.firstName}! </p>
                    <div style={{"clear": "both"}}></div>
                </div>
                
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
               
                {user.role == "advertiser" &&
                    <div className="container-fluid">
                    <div className="row">
                    <div className="col-sm">
                        <h2>List of Ad Units</h2>
                        <table><tbody>
                            {this.state.adUnits.map((adUnit, index) =>
                                <tr key={adUnit.adID + index}><td>
                                    {(new Date(adUnit.createdDate)).toLocaleDateString()}
                                </td><td>
                                    {adUnit.title}
                                </td>
                                <td>
                                    <button onClick={this.deleteAdUnit}>Delete</button>
                                </td>
                                </tr>
                            )}
                        </tbody></table>
                    </div>
                    <div className="col-sm">
                        <h2>Create Ad Unit</h2>
                        <form name="form" onSubmit={this.handleSubmit}>
                            
                            <div className='form-group'>
                                <label htmlFor="title">Title</label>
                                <input type="text" className="form-control" name="title" value={adUnit.title} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="content">Content</label>
                                <input type="text" className="form-control" name="content" value={adUnit.content} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="image">Image URL</label>
                                <input type="text" className="form-control" name="image" value={adUnit.image} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="video">Video URL</label>
                                <input type="text" className="form-control" name="video" value={adUnit.video} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="url">URL</label>
                                <input type="text" className="form-control" name="url" value={adUnit.url} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="interests">Interests</label>
                                <Select name="interests" value={interests}
                                        onChange={this.handleDDChange}
                                        options={ interestsList } 
                                        isMulti />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="viewcallbackurl">View Callback URL</label>
                                <input type="text" className="form-control" name="viewcallbackurl" value={adUnit.viewcallbackurl} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="likecallbackurl">Like Callback URL</label>
                                <input type="text" className="form-control" name="likecallbackurl" value={adUnit.likecallbackurl} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="clickcallbackurl">Click Callback URL</label>
                                <input type="text" className="form-control" name="clickcallbackurl" value={adUnit.clickcallbackurl} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="basepayoutv">Base Payout for View</label>
                                <input type="text" className="form-control" name="basepayoutv" value={adUnit.basepayoutv} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="basepayoutl">Base Payout for Like</label>
                                <input type="text" className="form-control" name="basepayoutl" value={adUnit.basepayoutl} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="basepayoutc">Base Payout for Click</label>
                                <input type="text" className="form-control" name="basepayoutc" value={adUnit.basepayoutc} onChange={this.handleChange} />
                            </div>
                            
                            <div className="form-group">
                                <button className="btn btn-primary">Create</button>
                            </div>
                            
                        </form>
                    </div>
                    </div>
                    </div>
                }
                {
                    user.role == "user" && 
                    <div className="container-fluid">
                    <div className="row">
                    <div className="col-sm">
                            <h2>List of Ad Units based on your interest</h2>
                            <form><table><tbody>
                                {this.state.adUnitsBOUI.map((post, index) =>
                                    <tr key={post.adID + index}><td>
                                        {post.title} 
                                        </td>
                                        <td>
                                        ${post.basepayout} 
                                        </td>
                                    </tr>
                                )}
                            </tbody></table>
                            {/* <input type="button" onClick={this.addToCart} value="Add to cart"></input> */}
                            </form>
                        </div>
                        <div className="col-sm">
                            <h2>Customize</h2>
                        </div>
                    </div>
                    </div>
                }
                <p>
                    
                </p>
            </div>
        );
    }
}

function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}



const actionCreators = {
    getUsers: userActions.getAll,
    deleteUser: userActions.delete
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };