import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Select from 'react-select';

import { userActions } from '../_actions';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                firstName: '',
                lastName: '',
                username: '',
                password: '',
                address: '',
                role: 'user',
                interests: null
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleDDChange = this.handleDDChange.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleDDChange(value) {
        const { user } = this.state;
        
        this.setState({
            user: {
                ...user,
                interests: value
            }
        });
    }

    handleRadioChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        const processedUser = {...user};
        processedUser.interests = processedUser.interests.map((item, index) => item.value);
        console.log("satya:   "+JSON.stringify(processedUser));

        if (processedUser.firstName && processedUser.lastName && processedUser.username && processedUser.password) {
            this.props.register(processedUser);
        }
    }

    render() {
        const { registering  } = this.props;
        const { user, submitted } = this.state;
        const { interests } = this.state.user;
        const interestsList = [
            { label: "Adventure", value: "Adventure" },
            { label: "Art", value: "Art" },
            { label: "Pets", value: "Pets" },
            { label: "Books", value: "Books" },
            { label: "Education", value: "Education" },
            { label: "Fashion", value: "Fashion" },
            { label: "Fitness", value: "Fitness" },
            { label: "Food", value: "Food" },
            { label: "Gadgets", value: "Gadgets" },
            { label: "Gardening", value: "Gardening" },
            { label: "Games", value: "Games" },
            { label: "Health", value: "Health" },
            { label: "Innovation", value: "Innvation" },
            { label: "Jewellery", value: "Jewellery" },
            { label: "Knowledge", value: "Knowledge" },
            { label: "Learning", value: "Learning" },
            { label: "Music", value: "Music" },
            { label: "Nutrition", value: "Nutrition" },
            { label: "Outdoor", value: "Outdoor" },
            { label: "Photography", value: "Photography" },
            { label: "Sports", value: "Sports" },
            { label: "Technology", value: "Technology" },
            { label: "Travelling", value: "Travelling" },
            { label: "Writing", value: "Writing" },
          ];
          //<Select options={ interestsList } />
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Register</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !user.firstName ? ' has-error' : '')}>
                        <label htmlFor="firstName">First Name</label>
                        <input type="text" className="form-control" name="firstName" value={user.firstName} onChange={this.handleChange} />
                        {submitted && !user.firstName &&
                            <div className="help-block">First Name is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.lastName ? ' has-error' : '')}>
                        <label htmlFor="lastName">Last Name</label>
                        <input type="text" className="form-control" name="lastName" value={user.lastName} onChange={this.handleChange} />
                        {submitted && !user.lastName &&
                            <div className="help-block">Last Name is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" value={user.username} onChange={this.handleChange} />
                        {submitted && !user.username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} />
                        {submitted && !user.password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.address ? ' has-error' : '')}>
                        <label htmlFor="address">Address</label>
                        <input type="text" className="form-control" name="address" value={user.address} onChange={this.handleChange} />
                        {submitted && !user.address &&
                            <div className="help-block">Address is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.role ? ' has-error' : '')}>
                            <input type="radio" name="role" value="user" checked={user.role == 'user'} onChange={this.handleRadioChange} />
                            User
                            <input type="radio" name="role" value="advertiser"  checked={user.role == 'advertiser'} onChange={this.handleRadioChange} />
                            Advertiser
                        {submitted && !user.role &&
                            <div className="help-block">Role is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.interests ? ' has-error' : '')}>
                        <label htmlFor="interests">Interests</label>
                        <Select name="interests" value={interests}
                                onChange={this.handleDDChange} 
                                options={ interestsList } 
                                isMulti />
                        {submitted && !user.interests &&
                            <div className="help-block">Interests is required</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">Register</button>
                        {registering && 
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                        <Link to="/login" className="btn btn-link">Cancel</Link>
                    </div>
                </form>
            </div>
        );
    }
}

function mapState(state) {
    const { registering } = state.registration;
    return { registering };
}

const actionCreators = {
    register: userActions.register
}

const connectedRegisterPage = connect(mapState, actionCreators)(RegisterPage);
export { connectedRegisterPage as RegisterPage };